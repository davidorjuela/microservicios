function PersonalData({ info }) {

    let editorOwner = 'undefined';

    if (info.editor === 'VSCode') {
        editorOwner = 'Microsoft';
    }
    if (info.editor === 'Atom') {
        editorOwner = 'Github';
    }
    if (info.editor === 'Sublime') {
        editorOwner = 'SublimeHQ';
    }
    if (info.editor === 'Brackets') {
        editorOwner = 'Adobe';
    }
    console.log(info.editor +': The editor owner of ' + editorOwner);
    info.editor = info.editor +': The editor owner of ' + editorOwner
    return { info }
}

function PreferencesColor({ color, changeColor }) {
    if(changeColor){
        switch (color){
            case 'blanco':
                return 'negro';
            case 'negro':
                return 'blanco';
            case 'amarillo':
                return 'violeta';
            case 'rojo':
                return 'verde';
            case 'azul':
                return 'naranja'
            default:
                return color;
        }
    }
    else{
        return color
    }
}

module.exports = { PersonalData, PreferencesColor }