const {PersonalData, PreferencesColor } = require('../Controllers')

function Servicio({ info, color}){
    let changeColor = false;
    if(info.edad > 18) changeColor = true;
    const personalData = PersonalData({ info });
    const preferencesColor = PreferencesColor({ color, changeColor });
    return { personalData, preferencesColor }
}

module.exports = { Servicio }