const { Adaptador } = require('./Adapters');

const result = Adaptador({
    color: 'dorado',
    info:{
        name: 'Pedro',
        edad: 34,
        domicilio: 'KR15 10-09, Granada-Cundinamarca',
        nacionalidad: 'colombiano',
        editor: 'VSCode',
    }
})
console.log(result);