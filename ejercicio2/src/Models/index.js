const db = [

    {
        id:1,
        info: { name: 'Pedro', edad: 17},
        color: 'rojo'
    },
    {
        id:2,
        info: { name: 'David', edad: 25},
        color: 'dorado'
    },
    {
        id:3,
        info: { name: 'Orjuela', edad: 34},
        color: 'azul'
    },

]

module.exports = { db }