const {queueView, queueDelete, queueUpdate, queueFindOne, queueCreate } = require('../src/Adapters/index');

async function Create({ name, age, color }){

    try {

        const job = await queueCreate.add({ name, age, color });
        const result = await job.finished();
        console.log(result);
        
    } catch (error) {

        console.log(error);

    }

}

async function Update({ name, age, color, id}){

    try {

        const job = await queueUpdate.add({ name, age, color, id});
        const result = await job.finished();
        console.log(result);
        
    } catch (error) {

        console.log(error);

    }

}

async function Delete({ id }){

    try {

        const job = await queueDelete.add({ id });
        const result = await job.finished();
        console.log(result);
        
    } catch (error) {

        console.log(error);

    }

}

async function FindOne({ name }){

    try {

        const job = await queueFindOne.add({ name });
        const result = await job.finished();
        console.log(result);
        
    } catch (error) {

        console.log(error);

    }

}

async function View({}){

    try {

        const job = await queueView.add({});
        const result = await job.finished();
        console.log(result);
        
    } catch (error) {

        console.log(error);

    }

}

async function main() {

    View({});
    
}

main();