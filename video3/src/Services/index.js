const Controllers = require('../Controllers');

async function Create ({ name, age, color }) {

    try {

        let { statusCode, data, message } = await Controllers.Create({ name, age, color });
        return { statusCode , data, message }
        
    } catch (error) {

        console.log({ step: 'service Create',  error: error.toString()});
        return { statusCode: 500, message: error.toString() }

    }

}

async function Update ({ name, age, color, id }) {

    try {

        let { statusCode, data, message } = await Controllers.Update({ name, age, color, id });
        return { statusCode , data, message }
        
    } catch (error) {

        console.log({ step: 'service Update',  error: error.toString()});
        return { statusCode: 500, message: error.toString() }

    }

}

async function Delete ({ id }) {

    try {

        let { statusCode, data, message } = await Controllers.Delete({ where: { id} });
        return { statusCode , data, message }
        
    } catch (error) {

        console.log({ step: 'service Delete',  error: error.toString()});
        return { statusCode: 500, message: error.toString() }

    }

}

async function FindOne ({ name }) {

    try {

        let { statusCode, data, message } = await Controllers.FindOne({ where: { name } });
        return { statusCode , data, message }
        
    } catch (error) {

        console.log({ step: 'service FindOne',  error: error.toString()});
        return { statusCode: 500, message: error.toString() }

    }

}

async function View ({ }) {

    try {

        let { statusCode, data, message } = await Controllers.View({ });
        return { statusCode , data, message }
        
    } catch (error) {

        console.log({ step: 'service View',  error: error.toString()});
        return { statusCode: 500, message: error.toString() }

    }

}

async function Servicio({ id }){

    try {

        const existUser = await ExistUser({ id });
        if(existUser.statusCode !== 200) throw (existUser.message);
        if(!existUser.data) throw ('No existe el usuario');

        const findUser = await FindUser({ id });
        if(findUser.statusCode !== 200) throw (findUser.message);

        if(findUser.data.info.edad > 18 ){

            console.log('Es mayor de edad');

        }

        return { statusCode:200 , data: findUser.data }

    } catch (error) {

        console.log({ step: 'service Servicio',  error: error.toString()});
        return { statusCode: 500, message: error.toString() }

    }
}

module.exports = { Create, Delete, Update, FindOne, View }