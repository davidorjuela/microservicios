const { Services } = require('../Services');
const { InternalError } = require('../settings');
const { queueView, queueDelete, queueUpdate, queueFindOne, queueCreate } = require('./index');

async function Create(job, done) {

    try {

        const { name, age, color } = job.data;

        let { statusCode, data, message } = await Services.Create({ name, age, color });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter Create',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function Delete(job, done) {

    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.DelequeueDelete({ id });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter Delete',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function Update(job, done) {

    try {

        const { name, age, color, id } = job.data;

        let { statusCode, data, message } = await Services.UpqueueUpdate({ name, age, color, id });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter Update',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function FindOne(job, done) {

    try {

        const { name } = job.data;

        let { statusCode, data, message } = await Services.FqueueFindOne({ name });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter FindOne',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function View(job, done) {

    try {

        const { } = job.data;

        let { statusCode, data, message } = await Services.View({ });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter View',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function run (){

    try {

        console.log('Worker inicializando...');
        queueCreate.process(Create);
        queueDelete.process(Delete);
        queueUpdate.process(Update);
        queueFindOne.process(FindOne);
        queueView.process(View);
        
    } catch (error) {

        console.log(error);
        
    }

}

module.exports = { Create, Delete, Update, FindOne, View, run }