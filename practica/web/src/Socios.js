import React, { useState, useEffect } from "react";
import { socket } from "./ws";
import styled from 'styled-components';

const Container = styled.div`
   position: relative;
   display: flex;
   flex-direction: row;
   flex-wrap: wrap;
   justify-content: space-evenly;
   align-self: center;
   align-content: stretch;
   width: 100%;
`
const Content =  styled.div`
   margin-top: 25px;
   display: flex;
   flex-direction: column;
   justify-content: stretch;
   max-height: 350px;
   overflow-y: scroll;
   overflow-x: hidden;
   width: 100%;
`
const Socio = styled.div`
   display: flex;
   flex-direction: column;
   border: 1px solid orange;
   margin: 10px 10px 0px 0px;
   padding: 15px;
`
const Row = styled.div`
   display: flex;
   justify-content: space-between;
   align-items: center;
   margin-top: 10px;
   position: relative;
`
const Name = styled.p`
   color: #333;
   font-weight: bold;
   font-size: 1.2rem;
   margin: 0px;
`
const Phone = styled.p`
   font-style: oblique;
   margin: 0px;
`
const Email = styled.p`
   color: #335;
   margin: 0px;
`
const Button = styled.button`
   background-color: #DDA;
   padding: 5px 10px;
   width: 100%;
   font-size: 1.1rem;
   font-weight: bolder;

`
const Enable = styled.div`
   width: 26px;
   height: 26px;
   border-radius: 50%;
   background-color:  ${props => props.enable ? 'green' : 'red'};
   opacity: 0.75;
   cursor: pointer;
   border: 2px solid #000;
`
const Icon = styled.img`
   width: 30px;
   height: 30px;
   border-radius: 50%;
   background-color: #200;
   margin-right: 10px;
   cursor: pointer;
   opacity: 0.75;
`
const FeedBack = styled.div`
   display: flex;
`
const SocioID = styled.small`
   position: absolute;
   top: -15px;
   right: 0px;
`

const App = () => {

   const [data, setData] = useState([]);

   useEffect(() => {

      socket.on('res:socios:view', ({ statusCode, data, message }) => {

         console.log('res:socios:view ',{ statusCode, data, message });
         
         if(statusCode === 200) setData(data);

      });

      setTimeout(() => socket.emit('req:socios:view', ({ enable: true })), 1000);

   }, []);

   const handleCreate = () => socket.emit('req:socios:create', {
      name: 'David', phone: '313 7516439', email: 'davidorjuel@gmail.com'
   });

   const handleDelete = (id) => socket.emit('req:socios:delete', { id });

   const handleChange = (id, status) => {
      if(status) socket.emit('req:socios:disable', { id });
      else socket.emit('req:socios:enable', { id });
   }

   return (
      <Container>
         <Button onClick={handleCreate}>Create Socio</Button>
         <Content>
         {
            data.map((v, i) => (
               <Socio>
                  <Row>
                     <SocioID>id:{v.id}</SocioID>
                     <Name>{v.name}</Name>
                     <Phone>{v.phone}</Phone>
                  </Row>
                  <Row>
                     <Email>{v.email}</Email>
                     <FeedBack>
                        <Icon src={'https://findicons.com/files/icons/1262/amora/128/delete.png'} 
                           onClick = {() => handleDelete(v.id)} />
                        <Enable enable={v.enable} onClick = {() => handleChange(v.id, v.enable) } />
                     </FeedBack>
                  </Row>
               </Socio>
            ))   
         }
         </Content>
      </Container>
   )

}

export default App;