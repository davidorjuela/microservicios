import React, { useState, useEffect } from "react";
import { socket } from "./ws";

const App = () => {

    const [id, setId] = useState();

    useEffect(() => setTimeout(() => setId(socket.id), 500), []);

    return(
        <p> { id? `Estás en linea ${id}` : 'Fuera de linea' } </p>
    )

}

export default App;