import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import Socios from './Socios'
import Libros from './Libros'
import Pagos from './Pagos'
import Footer from './Footer'

const Container = styled.div`
    display: flex;
    min-height: calc(100vh);
    margin: 0px;
    padding: 0px;
`

const Slyder = styled.div`
    background-color: #DDA;
    width: 35%;
    min-width: 350px;
    margin: 0px;
    padding: 15px;
`
const Body = styled.div`
    width: 65%;
    background-color: #553;
    margin: 0px;
    padding: 15px;
`

function App() {
    
    return (
        <Container>
            <Slyder>
                <Socios />
                <Pagos />
            </Slyder>
            <Body>
                <Libros />
            </Body>
        </Container>
    );
}

export default App;
