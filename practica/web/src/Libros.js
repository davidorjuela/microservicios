import React, { useState, useEffect } from "react";
import { socket } from "./ws";
import styled from "styled-components";

const Container = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
`
const Content =  styled.div`
    margin-top: 25px;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-items: flex-start;
    align-content: flex-start;
    gap: 20px;
`

const Libro = styled.div`
    background-color: #422;
    padding: 0px 12px 0px 25px;
    border-top-right-radius: 10px;
    border-right: 2px solid #886;
    position: relative;
    flex: 0 0;
`
const Portada = styled.img`
    height: 150px;
    width: auto;
    max-width: 120px;
`
const Icon = styled.img`
    width: 30px;
    height: 30px;
    position:absolute;
    top: -7px;
    left: -7px;
    border-radius: 50%;
    background-color: #200;
    opacity: 0.75;
    cursor: pointer;
`
const Title = styled.p`
    color: #AA8;
    font-weight: bold;
    text-align: center;
`
const Button = styled.button`
    background-color: #DDA;
    padding: 5px 10px;
    width: 100%;
    font-size: 1.1rem;
    font-weight: bolder;
`

const Item = ({ image, title, id }) => {

    const handleDelete = () => socket.emit('req:libros:delete', { id });
    const opts ={
        src: 'https://findicons.com/files/icons/1262/amora/128/delete.png',
        onClick: handleDelete
    }

    return (
        <Libro>
            <Icon {...opts} />
            <Portada src={image} />
            <Title>{title}</Title>
        </Libro>
    )
}

const App = () => {

    const [data, setData] = useState([]);
    
        useEffect(() => {
    
            socket.on('res:libros:view', ({ statusCode, data, message }) => {
    
            console.log('res:libros:view ',{ statusCode, data, message });
            
            if(statusCode === 200) setData(data);
    
            });
    
            setTimeout(() => socket.emit('req:libros:view', ({})), 1000);
    
        }, []);

    const handleCreate = () => socket.emit('req:libros:create', {
        image: 'https://nodejs.org/static/images/logo.svg', title: 'Aprende nodejs'
    });

    return(
        <Container>
            <Button onClick={handleCreate}>Create Libro</Button>
            <Content>
                { data.map((v, i) => <Item {...v} />)}
            </Content>
        </Container>
    )
}

export default App;