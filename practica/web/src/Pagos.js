import React, { useState, useEffect } from "react";
import { socket } from "./ws";
import styled from 'styled-components';

const Container = styled.div`
   position: relative;
   display: flex;
   flex-direction: row;
   flex-wrap: wrap;
   justify-content: space-evenly;
   align-self: center;
   align-content: stretch;
   width: 100%;
   margin-top: 25px;
`
const Content =  styled.div`
   margin-top: 25px;
   display: flex;
   flex-direction: column;
   justify-content: stretch;
   max-height: 250px;
   overflow-y: scroll;
   overflow-x: hidden;
   width: 100%;
`
const Pago = styled.div`
   display: flex;
   flex-direction: column;
   border: 1px solid orange;
   margin: 10px 10px 0px 0px;
   padding: 15px;
`
const Row = styled.div`
   display: flex;
   justify-content: space-between;
   align-items: center;
   margin-top: 10px;
   position: relative;
`
const Name = styled.p`
   margin: 0px;
`
const Amount = styled.p`
   color: #333;
   font-weight: bold;
   font-style: oblique;
   font-size: 1.2rem;
   margin: 0px;
`
const CreatedAt = styled.p`
   color: #335;
   margin: 0px;
`
const Button = styled.button`
   background-color: #DDA;
   padding: 10px;
   width: 100%;
   font-size: .9rem;
   font-weight: bolder;
`
const Icon = styled.img`
   width: 30px;
   height: 30px;
   border-radius: 50%;
   background-color: #200;
   margin-right: 10px;
   cursor: pointer;
   opacity: 0.75;
`
const Input = styled.input`
   border: 1px solid greenyellow;
   width: 50px;
   margin-right: 5px;
   padding: 10px 5px;
   font-size: 1rem;
`
const PagoID = styled.small`
   position: absolute;
   font-size: 0.9rem;
   top: -15px;
   right: 0px;
`

const App = () => {

   const [data, setData] = useState([{id:2, socio:13, amount: 300, createAt: 'esta noche larga'}]);
   const [value, setValue] = useState();

   useEffect(() => {

      socket.on('res:pagos:view', ({ statusCode, data, message }) => {

         console.log('res:pagos:view ',{ statusCode, data, message });
         
         if(statusCode === 200) setData(data);

      });

      setTimeout(() => socket.emit('req:pagos:view', ({})), 1000);

   }, []);

   const handleCreate = () => value >0 ? socket.emit('req:pagos:create', {
      socio: value, amount: 300
   }): null ;

   const handleDelete = (id) => socket.emit('req:pagos:delete', { id });

   const handleInput = (e) => {
      setValue(e.target.value);
   }

   return (
      <Container>
            <Row>
               <Input type={'number'} onChange={handleInput}></Input>
               <Button onClick={handleCreate}>Aplicar pago para el socio {value || 'ninguno'}</Button>
            </Row>
         <Content>
         {
            data.map((v, i) => (
               <Pago>
                  <Row>
                     <PagoID>Ticket # {v.id}</PagoID>
                     <Amount>Monto: ${v.amount}</Amount>
                     <Name>Socio: {v.socio}</Name>
                  </Row>
                  <Row>
                     <CreatedAt>Fecha:{v.createdAt}</CreatedAt>
                     <Icon src={'https://findicons.com/files/icons/1262/amora/128/delete.png'} 
                           onClick = {() => handleDelete(v.id)} />
                  </Row>
               </Pago>
            ))   
         }
         </Content>
      </Container>
   )

}

export default App;