const Services = require('../Services');
const { InternalError } = require('../settings');
const { queueView, queueDelete, queueUpdate, queueFindOne, queueCreate, queueEnable, queueDisable } = require('./index');

async function Create(job, done) {

    try {

        const { name, phone } = job.data;

        let { statusCode, data, message } = await Services.Create({ name, phone });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter Create',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function Delete(job, done) {

    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.Delete({ id });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter Delete',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function Update(job, done) {

    try {

        const { name, age, id, phone, email } = job.data;

        let { statusCode, data, message } = await Services.Update({ name, age, id, phone, email });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter Update',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function FindOne(job, done) {

    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.FindOne({ id });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter FindOne',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function View(job, done) {

    try {

        const { } = job.data;

        console.log(job.id);

        let { statusCode, data, message } = await Services.View({ });

        done(null, { statusCode, data: data.map(u => ({ ...u.toJSON(), ...{ worker: job.id } })), message });

    } catch (error) {

        console.log({ step: 'adapter View',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function Enable(job, done) {

    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.Enable({ id });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter Enable',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function Disable(job, done) {

    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.Disable({ id });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter Disable',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function run (){

    try {

        console.log('Worker inicializando...');
        queueCreate.process(Create);
        queueDelete.process(Delete);
        queueUpdate.process(Update);
        queueFindOne.process(FindOne);
        queueView.process(View);
        queueEnable.process(Enable);
        queueDisable.process(Disable);
        console.log('Worker OK');
        
    } catch (error) {

        console.log(error);
        
    }

}

module.exports = { Create, Delete, Update, FindOne, View, Enable, Disable, run }