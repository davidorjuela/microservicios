const { sequelize, name } = require('../settings');
const { DataTypes } = require('sequelize');

const Model = sequelize.define(name,{

    name: { type: DataTypes.STRING },
    age: { type: DataTypes.BIGINT },
    email: { type: DataTypes.STRING },
    phone: { type: DataTypes.STRING },
    enable: { type: DataTypes.BOOLEAN, defaultValue: true }

}, { freezeTableName:true });

async function SyncDB() {

    try {
        
        console.log('DB inicializando...');
        await Model.sync({ logging: false, force: true });
        console.log('DB OK');
        return { statusCode: 200 , data: 'OK' }

    } catch (error) {

        console.log(error);
        return { statusCode: 500, message: error.toString() }
        
    }

}

module.exports = { Model, SyncDB }