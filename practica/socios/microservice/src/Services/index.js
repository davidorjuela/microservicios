const Controllers = require('../Controllers');
const { InternalError } = require('../settings');

async function Create ({ name, phone}) {

    try {

        let { statusCode, data, message } = await Controllers.Create({ name, phone});
        return { statusCode , data, message }
        
    } catch (error) {

        console.log({ step: 'service Create',  error: error.toString()});
        return { statusCode: 500, message: error.toString() }

    }

}

async function Update ({ name, age, email, phone, id }) {

    try {

        let { statusCode, data, message } = await Controllers.Update({ name, age, email, phone, id });
        return { statusCode , data, message }
        
    } catch (error) {

        console.log({ step: 'service Update',  error: error.toString()});
        return { statusCode: 500, message: error.toString() }

    }

}

async function Delete ({ id }) {

    try {

        const findOne = await Controllers.FindOne({ where: { id } });
        if( findOne.statusCode !== 200 ){

            switch (findOne.statusCode) {

                case 400: return { statusCode: 400, message: "No existe el usuario a eliminar" }
                default: return { statusCode: 500, message: InternalError }

            }


        } 
        let del = await Controllers.Delete({ where: { id } });
        if( del.statusCode === 200 )return { statusCode:200 , data:findOne.data }
        
        return { statusCode: 400, message: InternalError }
        
    } catch (error) {

        console.log({ step: 'service Delete',  error: error.toString()});
        return { statusCode: 500, message: error.toString() }

    }

}

async function FindOne ({ id }) {

    try {

        let { statusCode, data, message } = await Controllers.FindOne({ where: { id } });
        return { statusCode , data, message }
        
    } catch (error) {

        console.log({ step: 'service FindOne',  error: error.toString()});
        return { statusCode: 500, message: error.toString() }

    }

}

async function View ({ enable }) {

    try {

        var where = {};
 
        if(enable !== undefined) where.enable=enable;

        let { statusCode, data, message } = await Controllers.View({ where });
        return { statusCode , data, message }
        
    } catch (error) {

        console.log({ step: 'service View',  error: error.toString()});
        return { statusCode: 500, message: error.toString() }

    }

}

async function Enable ({ id }) {

    try {

        let { statusCode, data, message } = await Controllers.Enable({ id });
        return { statusCode , data, message }
        
    } catch (error) {

        console.log({ step: 'service Enable',  error: error.toString()});
        return { statusCode: 500, message: error.toString() }

    }

}

async function Disable ({ id }) {

    try {

        let { statusCode, data, message } = await Controllers.Disable({ id });
        return { statusCode , data, message }
        
    } catch (error) {

        console.log({ step: 'service Disable',  error: error.toString()});
        return { statusCode: 500, message: error.toString() }

    }

}

module.exports = { Create, Delete, Update, FindOne, View, Enable, Disable }