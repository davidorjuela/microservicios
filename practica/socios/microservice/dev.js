const uService = require('./src');

async function main(){

    try {

        const db = await uService.SyncDB();
        if (db.statusCode !== 200 ) throw db.message;
        await uService.run();
        
    } catch (error) {
        
        console.log(error);
        return { statusCode: 500, message: error.toString() }

    }

} 

main();