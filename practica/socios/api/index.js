const bull = require('bull');
const { name } = require('./package.json');

const redis = { host: '192.168.0.17', port:6379 };

const opts = { redis: { host: redis.host, port: redis.port } };

const queueCreate = bull(`${name.replace('api-','')}:create`, opts);
const queueDelete = bull(`${name.replace('api-','')}:delete`, opts);
const queueUpdate = bull(`${name.replace('api-','')}:update`, opts);
const queueFindOne = bull(`${name.replace('api-','')}:findOne`, opts);
const queueView = bull(`${name.replace('api-','')}:view`, opts);
const queueEnable = bull(`${name.replace('api-','')}:enable`, opts);
const queueDisable = bull(`${name.replace('api-','')}:disable`, opts);

async function Create({ name, phone }){

    try {

        const job = await queueCreate.add({ name, phone });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message }
        //statusCode === 200 ? console.log('Bienvenido(a): ', data.name ) : console.error(message);
        
    } catch (error) {

        console.log(error);

    }

}

async function Update({ name, age, email, phone, id}){

    try {

        const job = await queueUpdate.add({ name, age, email, phone, id});
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message }
        //console.log({ statusCode, data, message });
        
    } catch (error) {

        console.log(error);

    }

}

async function Delete({ id }){

    try {

        const job = await queueDelete.add({ id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message }
        //statusCode === 200 ? console.log('Usuario eliminado: ', data.name ) : console.error(message);
        
    } catch (error) {

        console.log(error);

    }

}

async function FindOne({ id }){

    try {

        const job = await queueFindOne.add({ id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message }
        //statusCode === 200 ? console.log('Usuario buscado: ', data.name ) : console.error(message);
        
    } catch (error) {

        console.log(error);

    }

}

async function View({ enable }){

    try {

        const job = await queueView.add({ enable });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message }

        //if (statusCode === 200 ) for( let x of data ) console.log(x);
        //else console.error(message);
        
    } catch (error) {

        console.log(error);

    }

}

async function Enable({ id }){

    try {

        const job = await queueEnable.add({ id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message }
        
    } catch (error) {

        console.log(error);

    }

}

async function Disable({ id }){

    try {

        const job = await queueDisable.add({ id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message }
        
    } catch (error) {

        console.log(error);

    }

}

module.exports = { Create, Update, Delete, FindOne, View, Enable, Disable }