const api = require('./api');

async function main(){

    try {

        let { data, statusCode, message } = await api.Update(
            { name: 'Pedro', phone: '300 1234567', email: 'davidorjuela@gmail.com', enable: true, id: 22 }
        );

        console.log({ data, statusCode, message });

    } catch (error) {

        console.log(error);

    }
}

main();