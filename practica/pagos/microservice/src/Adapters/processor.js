const Services = require('../Services');
const { InternalError } = require('../settings');
const { queueView, queueDelete, queueUpdate, queueFindOne, queueCreate } = require('./index');

async function Create(job, done) {

    try {

        const { socio, amount } = job.data;

        let { statusCode, data, message } = await Services.Create({ socio, amount });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter Create',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function Delete(job, done) {

    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.Delete({ id });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter Delete',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function FindOne(job, done) {

    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.FindOne({ id });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter FindOne',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function View(job, done) {

    try {

        const { } = job.data;

        console.log(job.id);

        let { statusCode, data, message } = await Services.View({ });

        done(null, { statusCode, data: data.map(u => ({ ...u.toJSON(), ...{ worker: job.id } })), message });

    } catch (error) {

        console.log({ step: 'adapter View',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function run (){

    try {

        console.log('Worker inicializando...');
        queueCreate.process(Create);
        queueDelete.process(Delete);
        queueFindOne.process(FindOne);
        queueView.process(View);
        console.log('Worker OK');
        
    } catch (error) {

        console.log(error);
        
    }

}

module.exports = { Create, Delete, FindOne, View, run }