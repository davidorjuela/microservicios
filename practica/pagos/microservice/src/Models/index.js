const { sequelize, name } = require('../settings');
const { DataTypes } = require('sequelize');

const Model = sequelize.define(name,{

    socio: { type: DataTypes.BIGINT},
    amount: { type: DataTypes.BIGINT }

}, { freezeTableName:true });

async function SyncDB() {

    try {

        await Model.sync({ logging: false, force: true });
        console.log('DB OK'); 
        return { statusCode: 200 , data: 'OK' }

    } catch (error) {

        console.log(error);
        return { statusCode: 500, message: error.toString() }
        
    }

}

module.exports = { Model, SyncDB }