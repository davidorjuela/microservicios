const bull = require('bull');
const { name } = require('./package.json');

const redis = { host: '192.168.0.17', port:6379 };

const opts = { redis: { host: redis.host, port: redis.port } };

const queueCreate = bull(`${name.replace('api-','')}:create`, opts);
const queueDelete = bull(`${name.replace('api-','')}:delete`, opts);
const queueUpdate = bull(`${name.replace('api-','')}:update`, opts);
const queueFindOne = bull(`${name.replace('api-','')}:findOne`, opts);
const queueView = bull(`${name.replace('api-','')}:view`, opts);

async function Create({ title, image }){

    try {

        const job = await queueCreate.add({ title, image });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message }

    } catch (error) {

        console.log(error);

    }

}

async function Update({ title, category, seccion, id}){

    try {

        const job = await queueUpdate.add({ title, category, seccion, id});
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message }
        
    } catch (error) {

        console.log(error);

    }

}

async function Delete({ id }){

    try {

        const job = await queueDelete.add({ id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message }
        
    } catch (error) {

        console.log(error);

    }

}

async function FindOne({ tile }){

    try {

        const job = await queueFindOne.add({ tile });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message }
        //statusCode === 200 ? console.log('Usuario buscado: ', data.name ) : console.error(message);
        
    } catch (error) {

        console.log(error);

    }

}

async function View({}){

    try {
        
        const job = await queueView.add({});
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message }
        
    } catch (error) {

        console.log(error);

    }

}

module.exports = { Create, Update, Delete, FindOne, View }