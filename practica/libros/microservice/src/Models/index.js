const { sequelize, name } = require('../settings');
const { DataTypes } = require('sequelize');

const Model = sequelize.define(name,{

    title: { type: DataTypes.STRING },
    category: { type: DataTypes.BIGINT },
    seccion: { type: DataTypes.STRING },
    image: { type: DataTypes.STRING }

}, { freezeTableName:true });

async function SyncDB() {

    try {
        
        await Model.sync({ logging: false });
        console.log('DB OK');
        return { statusCode: 200 , data: 'OK' }

    } catch (error) {

        console.log(error);
        return { statusCode: 500, message: error.toString() }
        
    }

}

module.exports = { Model, SyncDB }