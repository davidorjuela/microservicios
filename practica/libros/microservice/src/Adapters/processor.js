const Bull = require('bull');
const Services = require('../Services');
const { InternalError } = require('../settings');
const { queueView, queueDelete, queueUpdate, queueFindOne, queueCreate } = require('./index');

async function Create(job, done) {

    try {

        const { title, image } = job.data;

        let { statusCode, data, message } = await Services.Create({ title, image });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter Create',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function Delete(job, done) {

    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.Delete({ id });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter Delete',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function Update(job, done) {

    try {

        const { id, title, category, seccion } = job.data;

        let { statusCode, data, message } = await Services.Update({ id, title, category, seccion });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter Update',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function FindOne(job, done) {

    try {

        const { title } = job.data;

        let { statusCode, data, message } = await Services.FindOne({ title });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter FindOne',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function View(job, done) {

    try {

        const { } = job.data;

        let { statusCode, data, message } = await Services.View({ });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter View',  error: error.toString()});

        done(null, { statusCode: 500, message: InternalError} );

    }

};

async function run (){

    try {

        console.log('Worker inicializando...');
        queueCreate.process(Create);
        queueDelete.process(Delete);
        queueUpdate.process(Update);
        queueFindOne.process(FindOne);
        queueView.process(View);
        console.log('Worker OK');
        
    } catch (error) {

        console.log(error);
        
    }

}

module.exports = { Create, Delete, Update, FindOne, View, run }