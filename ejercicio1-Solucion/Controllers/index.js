function PersonalData({ info }) {
    try {
        let editorOwner = 'undefined';

        if (info.editor === 'VSCode') editorOwner = 'Microsoft';
        if (info.editor === 'Atom') editorOwner = 'Github';
        if (info.editor === 'Sublime') editorOwner = 'SublimeHQ';
        if (info.editor === 'Brackets') editorOwner = 'Adobe';

        info.editor = info.editor +': The editor owner of ' + editorOwner;

        console.log(info.editor);

        return { statusCode: 200, data: info }

    } catch (error) {

        console.log({ step: 'controller PersonalData',  error: error.toString()});
        return { statusCode: 500, message: error.toString() }

    }
}

function PreferencesColor({ color }) {
    try {

        let c = color;

        if(color === 'blanco')  c = 'negro';
        if(color === 'negro')  c = 'blanco';
        if(color === 'amarillo')  c = 'violeta';
        if(color === 'rojo')  c = 'verde';
        if(color === 'azul')  c = 'naranja'

        return { statusCode: 200, data: c};

    } catch (error) {

        console.log({ step: 'controller PreferencesColor',  error: error.toString()});
        return { statusCode: 500, message: error.toString() }

    }
}

module.exports = { PersonalData, PreferencesColor }