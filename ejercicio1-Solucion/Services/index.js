const {PersonalData, PreferencesColor } = require('../Controllers')

function Servicio({ info, color}){

    try {

        const personalData = PersonalData({ info });
        //throw('Mensaje de error');
        if(info.edad < 18) throw('Necesitas ser mayor de edad!');

        const preferencesColor = PreferencesColor({ color });

        return { statusCode: 200, data: { personalData, preferencesColor} }

    } catch (error) {

        console.log({ step: 'service Servicio',  error: error.toString()});
        return { statusCode: 500, message: error.toString() }

    }
}

module.exports = { Servicio }