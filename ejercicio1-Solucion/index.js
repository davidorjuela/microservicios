const { Adaptador } = require('./Adapters');

const main = () => {
    try {
        const result = Adaptador({
            color: 'negro',
            info:{
                name: 'Pedro',
                edad: 19,
                domicilio: 'KR15 10-09, Granada-Cundinamarca',
                nacionalidad: 'colombiano',
                editor: 'VSCode',
            }
        })
        if (result.statusCode!==200) throw(result.message);
        console.log('Tu data es: ', result.data);



    } catch (error) {
        console.log(error);
    }
}

main();
