const { sequelize } = require('../settings');
const { DataTypes } = require('sequelize');

const Model = sequelize.define('curso',{

    name: { type: DataTypes.STRING },
    age: { type: DataTypes.BIGINT },
    color: { type: DataTypes.STRING }

});

async function SyncDB() {

    try {
        
        console.log('DB inicializando...');
        await Model.sync({ logging: false });
        console.log('DB OK');
        return { statusCode: 200 , data: 'OK' }

    } catch (error) {

        console.log(error);
        return { statusCode: 500, message: error.toString() }
        
    }

}

module.exports = { Model, SyncDB }